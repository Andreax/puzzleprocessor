package se.alsterit;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class MorphTest {
    @Test
    public void dilateTest() {
        int[][] testImage = {{0,0,0,0,0},
                             {0,0,0,0,0},
                             {0,0,1,0,0},
                             {0,0,0,0,0},
                             {0,0,0,0,0}};
        int[][] expectedImage = {{0,0,0,0,0},
                                  {0,2,2,2,0},
                                  {0,2,1,2,0},
                                  {0,2,2,2,0},
                                  {0,0,0,0,0}};
        Morph.dilate(testImage, 1);
        assertTrue(imagesEqual(testImage, expectedImage,1));
    }

    @Test
    public void dilateSmallTest() {
        int[][] testImage = {{0,0,0},
                             {0,1,0},
                             {0,0,0}};
        int[][] expectedImage = {      {2,2,2},
                                       {2,1,2},
                                       {2,2,2}};
        Morph.dilate(testImage, 1);
        assertTrue(imagesEqual(testImage, expectedImage,1));
    }

    @Test
    public void dilateMediumTest() {
        int[][] testImage = {{0,0,0,0,0,0,0,0,0},
                             {0,0,0,0,0,0,0,0,0},
                             {0,0,0,0,0,0,0,0,0},
                             {0,0,0,0,0,0,0,0,0},
                             {0,0,0,1,1,1,0,0,0},
                             {0,0,0,1,1,0,0,0,0},
                             {0,0,0,1,0,0,0,0,0},
                             {0,0,0,0,0,0,0,0,0},
                             {0,0,0,0,0,0,0,0,0}};
        int[][] expectedImage =  {{0,0,0,0,0,0,0,0,0},
                                  {0,0,0,0,0,0,0,0,0},
                                  {0,0,0,0,0,0,0,0,0},
                                  {0,0,2,2,2,2,2,0,0},
                                  {0,0,2,1,1,1,2,0,0},
                                  {0,0,2,1,1,2,2,0,0},
                                  {0,0,2,1,2,2,0,0,0},
                                  {0,0,2,2,2,0,0,0,0},
                                  {0,0,0,0,0,0,0,0,0}};
        Morph.dilate(testImage, 1);
        assertTrue(imagesEqual(testImage, expectedImage,1));
    }


    @Test
    public void dilateMediumTwoSizeTest() {
        int[][] testImage = {{0,0,0,0,0,0,0,0,0},
                             {0,0,0,0,0,0,0,0,0},
                             {0,0,0,0,0,0,0,0,0},
                             {0,0,0,0,0,0,0,0,0},
                             {0,0,0,1,1,1,0,0,0},
                             {0,0,0,1,1,0,0,0,0},
                             {0,0,0,1,0,0,0,0,0},
                             {0,0,0,0,0,0,0,0,0},
                             {0,0,0,0,0,0,0,0,0}};
        int[][] expectedImage =  {{0,0,0,0,0,0,0,0,0},
                                  {0,0,0,0,0,0,0,0,0},
                                  {0,2,2,2,2,2,2,2,0},
                                  {0,2,2,2,2,2,2,2,0},
                                  {0,2,2,1,1,1,2,2,0},
                                  {0,2,2,1,1,2,2,2,0},
                                  {0,2,2,1,2,2,2,2,0},
                                  {0,2,2,2,2,2,2,0,0},
                                  {0,2,2,2,2,2,0,0,0}};
        Morph.dilate(testImage, 2);
        assertTrue(imagesEqual(testImage, expectedImage, 2));
    }


    @Test
    public void erodeMediumTwoSizeTest() {
        int[][] testImage =  {{0,0,0,0,0,0,0,0,0},
                                         {0,0,0,0,0,0,0,0,0},
                                         {0,1,1,1,1,1,1,1,0},
                                         {0,1,1,1,1,1,1,1,0},
                                         {0,1,1,1,1,1,1,1,0},
                                         {0,1,1,1,1,1,1,1,0},
                                         {0,1,1,1,1,1,1,1,0},
                                         {0,1,1,1,1,1,1,0,0},
                                         {0,1,1,1,1,1,0,0,0}};
        int[][] expectedImage = {{0,0,0,0,0,0,0,0,0},
                                        {0,0,0,0,0,0,0,0,0},
                                        {0,-1,-1,-1,-1,-1,-1,-1,0},
                                        {0,-1,-1,-1,-1,-1,-1,-1,0},
                                        {0,-1,-1,1,1,1,-1,-1,0},
                                        {0,-1,-1,1,1,-1,-1,-1,0},
                                        {0,-1,-1,1,-1,-1,-1,-1,0},
                                        {0,-1,-1,-1,-1,-1,-1,0,0},
                                        {0,-1,-1,-1,-1,-1,0,0,0}};
        Morph.erode(testImage, 2);
        assertTrue(imagesEqual(testImage, expectedImage, 2));
    }
    
    private boolean imagesEqual(int[][] result, int[][] expected, int morphSize) {
        if (result == expected)
            return true;

        if (result == null || expected == null)
            return false;

        if (result.length != expected.length)
            return false;

        for (int i = morphSize; i < result.length-morphSize; i++) {
            if (result[i].length != expected[i].length)
                return false;
                for (int j = morphSize; j < result.length-morphSize; j++) {
                    if (result[i][j] != expected[i][j]) {
                        System.out.print("Failing at [" + i + "][" + j + "]\n");
                    }
                    assertThat(result[i][j], is(expected[i][j]));
                }
        }
        return true;
    }
}
