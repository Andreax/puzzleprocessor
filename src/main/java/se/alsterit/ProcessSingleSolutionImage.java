package se.alsterit;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ProcessSingleSolutionImage {

    public static void main(String[] args) throws IOException {
        Path inputPath = Paths.get("C:\\data\\solutions\\input");
        Path outputPath = Paths.get("C:\\data\\solutions\\output");
        long t1 = System.currentTimeMillis();
        SolutionsProcessor.processImage(inputPath.resolve("IMG_7539.JPG"), outputPath);
        System.out.println("\nProcessed " + Files.walk(inputPath).count() + " files in " + String.valueOf((System.currentTimeMillis()-t1)/1000) + "s");
    }
}
