package se.alsterit;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.plugins.jpeg.JPEGImageWriteParam;
import javax.imageio.stream.FileImageOutputStream;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.file.Path;

public class Utils {
    private static final int MORPH_SIZE = 3;
    public static final int END_MARKER_THRESHOLD = 3;

    public static BufferedImage openJpg(Path inputFile) {
        BufferedImage img = null;
        try {
            img = ImageIO.read(inputFile.toFile());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return img;
    }

    public static void saveJpeg(String fileName, Path outputPath, BufferedImage img) {
        try {
            Path outputFile = outputPath.resolve(fileName);

            JPEGImageWriteParam jpegParams = new JPEGImageWriteParam(null);
            jpegParams.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
            jpegParams.setCompressionQuality(0.7f);
            final ImageWriter writer = ImageIO.getImageWritersByFormatName("jpg").next();
            writer.setOutput(new FileImageOutputStream(outputFile.toFile()));
            writer.write(null, new IIOImage(img, null, null), jpegParams);
            writer.dispose();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static BufferedImage cropFrameMarkers(BufferedImage img, final double ratio) {
        int[] histX = new int[img.getWidth()];
        int[] histY = new int[img.getHeight()];
        histFilter(img, histX, histY);
        Size roi = new Size();
        roi.startX=findStart(histX);
        roi.stopX=findStop(histX);
        roi.startY=findStart(histY);
        roi.stopY=findStop(histY);
        roi.calcWidthAndHeight();
        return crop(img, ratio, roi);
    }

    private static void histFilter(BufferedImage img, int[] histX, int[] histY) {
        int r;
        int g;
        int b;
        for (int y=0;y<img.getHeight();y++) {
            for (int x=0;x<img.getWidth();x++) {
                int rgb = img.getRGB(x, y);
                r = (rgb & 0x00ff0000) >> 16;
                g = (rgb & 0x0000ff00) >> 8;
                b = (rgb & 0x000000ff);
                if (r>140 && (r==255 || r-b>40) && r-g>80) {
                    histX[x]++;
                }
                if (g>140 && (g==255 || g-r>20) && g-b>80) {
                    histY[y]++;
                }
            }
        }
    }

    public static BufferedImage crop(BufferedImage img, double ratio, Size roi) {
        Size newRoi = new Size();
        if (roi.height*ratio > roi.width) {
            // Extend width
            newRoi.startY = roi.startY;
            int approxNewObjectWidth = (int) (roi.height * ratio);
            newRoi.height = (int) (approxNewObjectWidth / ratio);
            newRoi.width = (int) (newRoi.height * ratio);
            newRoi.startX = roi.startX - (newRoi.width - roi.width)/2;
            newRoi.calcStop();
            if ((newRoi.startX<0) || (newRoi.stopX)>img.getWidth()) {
                img = extendCanvas(img, roi, newRoi);
            } else {
                clearArea(img, newRoi.startX, roi.startX, newRoi.startY, newRoi.startY + newRoi.height);
                clearArea(img, roi.stopX, newRoi.startX + newRoi.width, newRoi.startY, newRoi.startY + newRoi.height);
                img = img.getSubimage(newRoi.startX, newRoi.startY, newRoi.width, newRoi.height);
            }
        } else {
            // Extend height
            newRoi.startX = roi.startX;
            newRoi.height = (int) (roi.width / ratio);
            newRoi.width = (int) (newRoi.height * ratio);
            newRoi.startY = roi.startY - (newRoi.height - roi.height)/2;
            newRoi.calcStop();
            if ((newRoi.startY<0) || (newRoi.stopY)>img.getHeight()) {
                img = extendCanvas(img, roi, newRoi);
            } else {
                clearArea(img, newRoi.startX, newRoi.startX + newRoi.width, newRoi.startY, roi.startY);
                clearArea(img, newRoi.startX, newRoi.startX + newRoi.width, roi.stopY, newRoi.startY + newRoi.height);
                img = img.getSubimage(newRoi.startX, newRoi.startY, newRoi.width, newRoi.height);
            }
        }
        return img;
    }

    private static BufferedImage extendCanvas(BufferedImage img, Size roi, Size newRoi) {
        BufferedImage imgNew = new BufferedImage(newRoi.width, newRoi.height, img.getType());
        clearArea(imgNew, 0, imgNew.getWidth(), 0, imgNew.getHeight());
        copyArea(imgNew, img, roi);
        return imgNew;
    }

    /**
     * Copy the region of interest to a centered position in the provided new image.
     */
    private static void copyArea(BufferedImage imgNew, BufferedImage img, Size roi) {
        int oxStart=Math.max(0, (imgNew.getWidth() - roi.width) / 2);
        int oyStart=Math.max(0, (imgNew.getHeight() - roi.height) / 2);
        for (int iy=roi.startY, oy=oyStart; iy < roi.stopY; iy++, oy++) {
            for (int ix = roi.startX, ox=oxStart; ix < roi.stopX; ix++, ox++) {
                imgNew.setRGB(ox, oy, img.getRGB(ix,iy));
            }
        }
    }

    private static void clearArea(BufferedImage img, int startX, int stopX, int startY, int stopY) {
        for (int y = startY; y < stopY; y++) {
            for (int x = startX; x < stopX; x++) {
                img.setRGB(x, y, 0xffffffff);
            }
        }
    }

    private static int findStart(int[] hist) {
        int start;
        for (start=1;start<hist.length;start++) {
            if (END_MARKER_THRESHOLD >=hist[start] && END_MARKER_THRESHOLD <hist[start-1]) {
                return start + 10;// Cut 10 pixels extra
            }
        }
        return 0;
    }

    private static int findStop(int[] hist) {
        int stop;
        for (stop=hist.length-2;stop>0;stop--) {
            if (END_MARKER_THRESHOLD >= hist[stop] && END_MARKER_THRESHOLD < hist[stop+1]) {
                return stop - 10;// Cut 10 pixels extra
            }
        }
        return hist.length;
    }

    public static BufferedImage resize(BufferedImage originalImage, int width, int height) {
        BufferedImage resizedImage = new BufferedImage(width, height, originalImage.getType());
        Graphics2D g = resizedImage.createGraphics();
        g.drawImage(originalImage, 0, 0, width, height, null);
        g.dispose();
        return resizedImage;
    }

    public static void thresholding(BufferedImage img, int[][] imageMask) {
        Morph.dilate(imageMask, MORPH_SIZE);
        Morph.flat(imageMask);
        Morph.erode(imageMask, 2*MORPH_SIZE);
        Morph.flat(imageMask);
        Morph.dilate(imageMask, MORPH_SIZE);
        for (int y=0;y<img.getHeight();y++) {
            for (int x = 0; x < img.getWidth(); x++) {
                if (0 >= imageMask[MORPH_SIZE + x][MORPH_SIZE + y]) {
                    img.setRGB(x, y, 0xffffff);
                }
            }
        }
    }

    public static int[][] rgbFilter(BufferedImage img) {
        int[][] imageMask = new int[img.getWidth()+MORPH_SIZE][img.getHeight()+MORPH_SIZE];
        int r;
        int g;
        int b;
        for (int y=0;y<img.getHeight();y++) {
            for (int x=0;x<img.getWidth();x++) {
                int rgb = img.getRGB(x, y);
                r = (rgb & 0x00ff0000) >> 16;
                g = (rgb & 0x0000ff00) >> 8;
                b = (rgb & 0x000000ff);
                int max = Math.max(Math.max(r,b), g);
                int med = Math.min(Math.max(r,b), g);
                int min = Math.min(Math.min(r,b), g);
// Regular / Dark
                if ((b==max && b>(min+11) && b>(med+5) && b>165) || (b>(min+20) &&  b>235) || (b>250)) {
// Light tree puzzles

                // Puzzle: 195, 195, 207
                // Background : 164, 198, 223
//                if (b>210 && (g>200 || (g>170 && (b-r>15)))) {
                    // background
                    imageMask[MORPH_SIZE + x][MORPH_SIZE + y] = 0;
                } else {
                    // puzzle
                    imageMask[MORPH_SIZE + x][MORPH_SIZE + y] = 1;
                }
            }
        }
        return imageMask;
    }

    public static BufferedImage cropPuzzle(BufferedImage img) {
        int[] histX = new int[img.getWidth()];
        int[] histY = new int[img.getHeight()];
        puzzleHistFilter(img, histX, histY);
        int[] startAndStop = new int[2];
        findPuzzleStartAndStop(histX, startAndStop);
        int startX = startAndStop[0];
        int stopX = startAndStop[1];
        findPuzzleStartAndStop(histY, startAndStop);
        int startY = startAndStop[0];
        int stopY = startAndStop[1];
        if (startX!=0 && stopX!=0){
            img = img.getSubimage(startX, startY, stopX - startX, stopY - startY);
        } else {
            System.out.println("WARNING: could not puzzle crop current file. ");
        }
        return img;
    }

    public static void puzzleHistFilter(BufferedImage img, int[] histX, int[] histY) {
        for (int y=0;y<img.getHeight();y++) {
            for (int x=0;x<img.getWidth();x++) {
                int rgb = img.getRGB(x, y);
                if (0xffffffff!=rgb) {
                    histX[x]++;
                    histY[y]++;
                }
            }
        }
    }

    private static void findPuzzleStartAndStop(int[] hist, int[] startAndStop) {
        int start = 0;
        int stop = 0;
        int sectionStart = 0;
        for (int i=1;i<hist.length;i++) {
            if (0 < hist[i] && 0 == hist[i-1]) {
                sectionStart = i;
            }
            if (0 == hist[i] && 0 < hist[i-1]) {
                int sectionStop = i;
                if (20 < (sectionStop - sectionStart)) {
                    start = (0==start)?sectionStart:start;
                    stop = sectionStop;
                }
            }
        }
        startAndStop[0] = start;
        startAndStop[1] = stop;
    }

    static long lastTime = 0;
    public static void printTime(String message) {
        long timeNow = System.currentTimeMillis();
        System.out.println(message + ": " + (timeNow - lastTime) + "ms");
        lastTime = timeNow;
    }
}
