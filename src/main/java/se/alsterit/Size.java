package se.alsterit;

public class Size {
    int width;
    int height;
    int startX;
    int stopX;
    int startY;
    int stopY;

    public Size() {}

    public Size(int startX, int width, int startY, int height) {
        this.startX = startX;
        this.width = width;
        this.startY = startY;
        this.height = height;
        calcStop();
    }

    public void calcWidthAndHeight() {
        width = stopX-startX+1;
        height = stopY-startY+1;
    }

    public void calcStop() {
        stopX = startX+width-1;
        stopY=startY+height-1;
    }
}
