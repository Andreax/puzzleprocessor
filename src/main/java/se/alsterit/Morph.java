package se.alsterit;

public class Morph {

    public static void flat(int[][] image) {
        for (int y=0; y<image[0].length; y++) {
            for (int x = 0; x < image.length; x++) {
                if (0<image[x][y]) {
                    image[x][y] = 1;
                } else {
                    image[x][y] = 0;
                }
            }
        }
    }

     /**
     * @param image image to process including a morph frame. Image must contain only 1 and 0.
     * @param morphSize radius of the morph square. Morph Square = (morphSize-1)/2
     */
    public static void dilate(int[][] image, final int morphSize) {
        morph(image, morphSize, 1, 0, -1);
    }

    public static void erode(int[][] image, final int morphSize) {
        morph(image, morphSize, 0, 1, 2);
    }

    private static void morph(int[][] image, final int morphSize, int change, int search, int set) {
        for (int y=morphSize; y<image[0].length-morphSize; y++) {
            boolean morph = true;
            for (int x=morphSize; x<image.length-morphSize; x++) {
                int morphX;
                if (change == image[x][y]) {
                    int morphXTo = morph? -morphSize : morphSize;
                    morph=false;
                    for (morphX = morphSize; morphX >= morphXTo; morphX--) {
                        for (int morphY = -morphSize; morphY <= morphSize; morphY++) {
                            if (search == image[x + morphX][y + morphY]) {
                                morph = true;
                                break;
                            }
                        }
                        if (morph) {
                            break;
                        }
                    }
                    if (morph) {
                        int dilateToPixel = Math.min(x + morphX + morphSize, image.length-morphSize);
                        for (;x<=dilateToPixel;x++) {
                            if (change == image[x][y]) {
                                image[x][y] = set;
                            }
                        }
                        x--;
                    }
                }
            }
        }
    }
}