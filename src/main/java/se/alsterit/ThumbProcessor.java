package se.alsterit;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

import static se.alsterit.Utils.*;
import static se.alsterit.Utils.printTime;
import static se.alsterit.Utils.saveJpeg;

public class ThumbProcessor {

    /**
     * Create thumbs for the first page using the previously processed main images.
     * Thumbs are 640x640 pixels.
     */
    public static void main(String[] args) throws IOException {
        Path inputPath = Paths.get("C:\\data\\data");
        Stream<Path> files = Files.walk(inputPath);
        long t1 = System.currentTimeMillis();
        files.parallel().filter(s -> s.getFileName().toString().equals("main.jpg")).forEach(s -> processImage(s));
        System.out.println("\nProcessed " + (Files.walk(inputPath).count() - 1) + " files in " + String.valueOf((System.currentTimeMillis() - t1) / 1000) + "s");
    }


    public static void processImage(Path inputFile) {
        Utils.printTime("Start Processing " + inputFile.toString());

        BufferedImage img = openJpg(inputFile);
        printTime(" - Open File");

        // Fix ratio 1:1
        img = crop(img, 1, new Size(0,img.getWidth(),0,img.getHeight()));
        printTime(" - Cropping");

        int imgLongestSide = Math.max(img.getHeight(), img.getWidth());
        int newSize = Math.min(imgLongestSide, 640);
        img = resize(img, newSize, newSize);
        printTime(" - Resizing");

        saveJpeg("main_thumb.jpg", inputFile.getParent(), img);
        printTime(" - Saving File");
    }

}
