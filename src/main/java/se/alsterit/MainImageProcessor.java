package se.alsterit;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

import static se.alsterit.Utils.*;

public class MainImageProcessor {

    /**
     * Process the main image and the pieces image used for the Pieces Tab
     * Each image is tightly cropped around the ROI
     */
    public static void main(String[] args) throws IOException {
        Path inputPath = Paths.get("C:\\data\\mainimage\\input");
        Path outputPath = Paths.get("C:\\data\\mainimage\\output");
        Stream<Path> files = Files.walk(inputPath);
        long t1 = System.currentTimeMillis();
        files.parallel().filter(s -> s.getFileName().toString().endsWith(".JPG")).forEach(s -> processImage(s, outputPath));
        System.out.println("\nProcessed " + (Files.walk(inputPath).count() -1) + " files in " + String.valueOf((System.currentTimeMillis()-t1)/1000) + "s");
    }

    public static void processImage(Path inputFile, Path outputPath) {
        Utils.printTime("Start Processing " + inputFile.toString());

        BufferedImage img = openJpg(inputFile);
        printTime(" - Open File");

        img = cropFrameMarkers(img, 2);
        printTime(" - Cropping");

        int[][] imageMask = rgbFilter(img);
        thresholding(img, imageMask);
        printTime(" - Morph and thresholding");

        img = cropPuzzle(img);
        printTime(" - Crop puzzle");

        saveJpeg(inputFile.getFileName().toString().toLowerCase(), outputPath, img);
        printTime(" - Saving File");
    }
}
