package se.alsterit;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

import static se.alsterit.Utils.*;
import static se.alsterit.Utils.printTime;

public class JpegCompressionProcessor {

    public static void main(String[] args) throws IOException {
        Path inputPath = Paths.get("C:\\data\\data");
        Stream<Path> files = Files.walk(inputPath);
        long t1 = System.currentTimeMillis();
        files.parallel().filter(s -> s.getFileName().toString().endsWith(".jpg")).forEach(s -> processImage(s));
        System.out.println("\nProcessed " + (Files.walk(inputPath).count() - 1) + " files in " + String.valueOf((System.currentTimeMillis() - t1) / 1000) + "s");
    }


    public static void processImage(Path inputFile) {
        Utils.printTime("Start Processing " + inputFile.toString());

        BufferedImage img = openJpg(inputFile);
        printTime(" - Open File");

        String fileName = inputFile.getFileName().toString();
        saveJpeg(fileName.substring(0, fileName.length()-1)+"x", inputFile.getParent(), img);
        printTime(" - Saving File");
    }
}
